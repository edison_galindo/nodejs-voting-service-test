var gulp = require('gulp'),
    apidoc = require('gulp-apidoc'),
    shell = require('gulp-shell');

gulp.task('default', function() {
    // place code for your default task here
});

gulp.task('apidoc', function(done){
    apidoc({
        src: "app/",
        dest: "public/api"
    }, done);
});

gulp.task('test',
  shell.task([
    'cd ./test;npm test'
  ], {
    ignoreErrors: true,
    env: 'test'
  }
));
