'use strict';
/* jshint undef: false */
/* jshint unused: false */
/* global describe it before async f request */

describe('User Controller', function () {
  var accessToken;

  before(function (done) {
    auth.getToken(f.user2, function(err, token){
      accessToken = token;
      done();
    });
  });

  it('Should create a user', function (done) {
    request
    .post('/api/v1/users')
    .send(f.user3)
    .expect(200)
    .end(done);
  });

  it('Should to get the user on session', function (done) {
    request
    .get('/api/v1/users/me')
    .set('token', accessToken)
    .expect(200)
    .end(done);
  });

  it('Get a list of available polls', function (done) {
    request
    .get('/api/v1/poll')
    .set('token', accessToken)
    .expect(200)
    .end(done);
  });

  it('Should fail to try to get a list of available polls.', function (done) {
    request
    .get('/api/v1/poll')
    .expect(401)
    .end(done);
  });

  
});