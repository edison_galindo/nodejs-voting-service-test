process.env.NODE_ENV = 'testing';

before(function(done) {
  this.timeout(1000000000);
  console.log('==================================');
  console.log('=|  _______  _____  _____       |=');
  console.log('=|  |_____| |_____]   |    test |=');
  console.log('=|  |     | |       __|__       |=');
  console.log('==================================');
  async = require('async');
  should = require('should');
  assert = require('assert');
  request = require('supertest');
  request = request('http://localhost:4000');
  winston = require('winston');
  f = {};
  auth = null;
  server = null;
  async.waterfall([
    function dropDatabase (cb) {
      var mongoose = require('../app/init/db').mongoose;
      mongoose.connection.on('open', function () {
        mongoose.connection.db.dropDatabase(function() { cb(); });
      });
    },
    function loadFixtures (cb) {
      var files = require('fs').readdirSync('./test/fixtures');
      files.forEach(function(file) {
        // require localized to this file
        f[file.slice(0, -3)] = require('./fixtures/' + file);
      });
      auth = require('./fixtures/_auth.js');
      server = require('../server');
      cb();
    },
    function generateData (cb) {
      f._generate.data(f, function(err){
        if (err)  {
          console.error('err', err);
          return cb(err);
        }
        cb(null, server);
      });
    }
  ], done);
});
