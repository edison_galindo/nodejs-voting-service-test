'use strict';
/* jshint undef: false*/
/* jshint unused: false */

module.exports = {
  getToken: getToken
};

function getToken (user, cb) {
  f.email = user.email;
  f.password = user.password;
  request
  .post('/api/v1/users/login')
  .expect(200)
  .send(f.clientCredentials)
  .end(function (err, response) {
    if (err) return cb(err);
    /*jshint ignore:start */

    cb(null, response.body.access_token);
    /* jshint ignore:end  */
  });
}
