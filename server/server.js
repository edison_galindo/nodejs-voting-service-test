//-----------------------Initialize DB-------------------------\\
var db = require('./app/init/db');
var expressInitializer = require('./app/init/express');
var bootstrap = require('./app/init/bootstrap')();

var server = expressInitializer();

module.exports = server;