/*!
 * ZEMOGA Poll API
 * Author(c) 2018 Edison Galindo <edison.galindo@gmail.com> 
 */
var ValidationService = require('../services/core/ValidationService');
var PollService = require('../services/PollService');
var User = require('../models/user').User;
var _ = require('lodash');
var u = require('../lib/utils');
var config = require('../../config/appConfig');
const i18n = require('i18n');
var PollController = {};

 /**
 * @api {post} /api/v1/poll Create
 * @apiHeader {String} [token] user jwt token
 * @apiName Create
 * @apiGroup Poll
 * @apiDescription Create a poll item record
 * @apiVersion 1.0.0
 *
 * @apiParam {string} [title] Name of the person or item to vote.
 * @apiParam {string} [description] Short description about the person or item to vote.
 *
 * @apiParamExample {json} Request-Example:
 *  {
 *    "title" : "kayne West",
 *    "description" : "Who is this guy?"
 *  }
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data": {
 *      "cursor": 1,
 *      "title": "Kanye West",
 *      "description": "What is wrong with this guy?",
 *      "_id": "5bd4d3a812a9e26685887127",
 *      "updatedAt": "2018-10-27T21:07:52.564Z",
 *      "createdAt": "2018-10-27T21:07:52.564Z",
 *      "downTotal": 0,
 *      "upTotal": 0
 *    }
 *  }
 */
PollController.create = function(req, res) {

  ValidationService.validateInput(req.body, 'createPoll')
    .then(function() {
      PollService.create(req.body)
        .then((createdPoll) => {
          res.ok({data: createdPoll});
        })
        .catch((err) => {
          res.badRequest(err);
        });
    }).catch(function(err){
      res.badRequest(err)
    });

};

/**
 * @api {update} /api/v1/poll/:id Update
 * @apiHeader {String} [token] user jwt token
 * @apiName Update
 * @apiGroup Poll
 * @apiDescription Update a poll item record
 * @apiVersion 1.0.0
 *
 * @apiParam {string} [title] Name of the person or item to vote.
 * @apiParam {string} [description] Short description about the person or item to vote.
 *
 * @apiParamExample {json} Request-Example:
 *  {
 *    "title" : "kayne West",
 *    "description" : "Who is this guy?"
 *  }
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data": {
 *      "cursor": 1,
 *      "title": "Kanye West",
 *      "description": "What is wrong with this guy?",
 *      "_id": "5bd4d3a812a9e26685887127",
 *      "updatedAt": "2018-10-27T21:07:52.564Z",
 *      "createdAt": "2018-10-27T21:07:52.564Z",
 *      "downTotal": 0,
 *      "upTotal": 0
 *    }
 *  }
 */
PollController.update = function(req, res) {

  ValidationService.validateInput(req.body, 'updatePoll')
  .then(function() {
    PollService.update(req.body)
    .then((createdBatch) => {
      res.ok({data: createdBatch});
    })
    .catch((err) => {
      res.badRequest(err);
    });
  }).catch(function(err){
    res.badRequest(err)
  });

};

/**
 * @api {get} /api/v1/poll/:id Read a poll item
 * @apiHeader {String} [token] JWT token
 * @apiName read
 * @apiGroup Poll
 * @apiDescription Read an specific poll record by Id
 * @apiVersion 1.0.0
 *
 * @apiParam {String} [id] Poll item id
 *  
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data": {
 *      "_id": "5bd4d3a812a9e26685887127",
 *      "cursor": 1,
 *      "title": "Kanye West",
 *      "description": "What is wrong with this guy?",
 *      "updatedAt": "2018-10-27T21:07:52.564Z",
 *      "createdAt": "2018-10-27T21:07:52.564Z",
 *      "downTotal": 0,
 *      "upTotal": 0
 *    }
 *  }
 */
PollController.read = function(req, res) {

  ValidationService.validateInput({id:req.params.id}, 'readPoll')
  .then(function() {
    
    PollService.read(req.params.id)
    .then((poll) => {
      res.ok({data: poll});
    })
    .catch((err) => {
      res.badRequest(err);
    });

  }).catch(function(err){
    res.badRequest(err)
  });

};


/**
 * @api {get} /api/v1/poll All available poll items
 * @apiHeader {String} [token] User's jwt token
 * @apiName List
 * @apiGroup Poll
 * @apiDescription A collection of available poll items
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "data": {
 *      "docs": [
 *          {
 *              "upTotal": 4,
 *              "downTotal": 1,
 *              "_id": "5bd5e22e7f9befd7aab6fcdb",
 *              "cursor": 0,
 *              "title": "Mark Zuckerberg",
 *              "description": "Yeah yeah, we care about your privacy.",
 *              "updatedAt": "2018-10-28T23:34:23.500Z",
 *              "createdAt": "2018-10-28T16:22:06.214Z",
 *              "__v": 0
 *          },
 *          {
 *              "upTotal": 0,
 *              "downTotal": 1,
 *              "_id": "5bd5e23f7f9befd7aab6fcdc",
 *              "cursor": 1,
 *              "title": "Kanye West",
 *              "description": "What is wrong with this guy?",
 *              "updatedAt": "2018-10-28T23:35:04.201Z",
 *              "createdAt": "2018-10-28T16:22:23.230Z",
 *              "__v": 0
 *          }
 *      ],
 *      "total": 2,
 *      "limit": 10,
 *      "page": 1,
 *      "pages": 1
 *  }
 * }
 */
PollController.list = function(req, res) {

  PollService.list(req.query)
    .then((pollsList) => {
      res.ok({data: pollsList});
    })
    .catch((err) => {
      res.badRequest(err);
    });

};

/**
 * @api {post} /api/v1/poll/vote Vote
 * @apiHeader {String} [token] user jwt token
 * @apiName Vote
 * @apiGroup Poll
 * @apiDescription Vote up or down a poll item
 * @apiVersion 1.0.0
 *
 * @apiParam {string} [id] Poll Id
 * @apiParam {string} [type] 'up' or 'down' 
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data": {
 *     
 *    }
 *  }
 */
PollController.vote = function(req, res) {

  ValidationService.validateInput(req.body, 'vote')
    .then(function() {
      PollService.vote(req.body.id, req.body.type, req.user._id)
        .then((voteResult) => {
          if(voteResult === false){
            res.badRequest({message: i18n.__("max_of_votes")})
          } else {
            res.ok({data: voteResult});
          }
        })
        .catch((err) => {
          res.badRequest(err);
        });
    }).catch(function(err){
      res.badRequest(err)
    });

};

/**
 * @api {get} /api/v1/poll/:id/check Check if is able to vote
 * @apiHeader {String} [token] JWT token
 * @apiName check
 * @apiGroup Poll
 * @apiDescription Check if is able to vote a poll item
 * @apiVersion 1.0.0
 *
 * @apiParam {String} [id] Poll item id
 *  
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data": {
 *      "canVote": true 
 *    }
 *  }
 */
PollController.check = function(req, res) {

  ValidationService.validateInput({id:req.params.id}, 'checkVote')
  .then(function() {
    
    PollService.check(req.params.id, req.user._id)
    .then((result) => {
      if(!result) {
        res.ok({data: {canVote: false, message: ''}});
      } else {
        res.ok({data: {canVote: true, message: ''}});
      }
    })
    .catch((err) => {
      res.badRequest(err);
    });

  }).catch(function(err){
    res.badRequest(err)
  });

};

/**
 * @api {get} /api/v1/poll/my-votes All available poll with user votes
 * @apiHeader {String} [token] User's jwt token
 * @apiName ListMyVotes
 * @apiGroup Poll
 * @apiDescription A collection of available poll items with user votes
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "data": [
 *     {
 *         "id": "5bd5e22e7f9befd7aab6fcdb",
 *         "title": "Mark Zuckerberg",
 *         "description": "Yeah yeah, we care about your privacy.",
 *         "upTotal": 4,
 *         "downTotal": 2,
 *         "totalVotes": 6,
 *         "myVotes": 0
 *     },
 *     {
 *         "id": "5bd5e23f7f9befd7aab6fcdc",
 *         "title": "Kanye West",
 *         "description": "What is wrong with this guy?",
 *         "upTotal": 1,
 *         "downTotal": 2,
 *         "totalVotes": 3,
 *         "myVotes": 0
 *     }
 *  
 * }
 */
PollController.listWithVotes = function(req, res) {

  PollService.listWithVotes(req.query, req.user._id)
    .then((pollsList) => {
      res.ok({data: pollsList});
    })
    .catch((err) => {
      res.badRequest(err);
    });

};

module.exports = PollController;
