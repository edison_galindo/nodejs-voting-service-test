var validationService = require('../services/core/ValidationService');
var userService = require('../services/UserService');
var User = require('../models/user').User;
var _ = require('lodash');
var u = require('../lib/utils');
var config = require('../../config/appConfig');
var userCtrl = {};

 /**
 * @api {post} /api/v1/users User registration
 * @apiName Create
 * @apiGroup User
 * @apiDescription Registration of a User in the system
 * @apiVersion 1.0.0
 *
 * @apiParam {string} email user email.
 * @apiParam {string} password user password.
 * @apiParam {integer} age - We must to ask for the birthday, maybe?
 * @apiParam {string} marriage_status (Married, Single , Divorced, Widowed, Other)
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "password": "mySecurePassword!",
 *   "email": "whoiam@email.com",
 *   "age": 25,
 *   "marriage_status": "Sigle"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *   "email": "asdlkasldk@asdkjkda.com",
 *   "password": "$2a$10$NoJV5izPvB1HK2v./MgxC.x/8/wGUGmCf0SpIHBiPDdAHSyyzOwZa",
 *   "age": 25,
 *   "marriage_status": "Sigle"
 *   "_id": "56a7c125706144ab0b2ab7e9",
 *   "updatedAt": "2018-10-28T18:55:33.800Z",
 *   "createdAt": "2018-10-28T18:55:33.800Z",
 * }
 */
userCtrl.create = (req, res) => {
  
  validationService.validateInput(req.body, 'createUser')
  .then(function() {

    userService.createUser(req.body)
    .then((createdUser) => {
      res.ok({data: createdUser});
    })
    .catch((err) => {
      console.log('Err:::', err);
      if (typeof err == 'string') {
        res.badRequest(res.__(err));
      } else {
        res.badRequest(err);
      }
    });
    
  })  
  .catch(function(err){
    // console.log('validation error?', err)
    res.badRequest(err);
  });


  
};


/**
 * @api {post} /api/v1/users/login Login
 * @apiName Login
 * @apiGroup User
 * @apiDescription Email and password authentication to get a token
 * @apiVersion 1.0.0
 *
 * @apiParam {string} [email] User email.
 * @apiParam {string} [password] User password.
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "email": "my@email.com",
 *   "password": "mySecurePassword!"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "data": {
 *     "_id": "57af3e264336761409a35125",
 *     "firstName": "John",
 *     "lastName": "Reid",
 *     "password": "$2a$10$4iH/mFr54MUiLW8iqCu2/uehUghfFUg7G9IOSsH9xZfKFsVK79gnm",
 *     "email": "loneranger@gmail.com",
 *     "documentId": "12345678",
 *     "company": {
 *       "name": "ALGOGAN",
 *       "companyDocumentId": "12345678-6",
 *       "address": "Calle 123"
 *     },
 *     "__v": 0,
 *     "lastActivityDate": "2016-10-22T18:08:55.374Z",
 *     "updatedAt": "2016-08-13T15:35:02.215Z",
 *     "createdAt": "2016-08-13T15:35:02.214Z",
 *     "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImdhdG9kZXZlbG9wZXJAZ21haWwuY29tIiwiaWF0IjoxNDc3MTU5NzM1LCJleHAiOjE0Nzk3NTE3MzV9.mQIzHYCJo4tBib3jMLeahIyKZjvNOFruA5YrPDefUTY",
 *     "address": null,
 *     "image": null,
 *     "isDeleted": false,
 *     "isActive": true,
 *     "isValidated": true,
 *     "role": "User"
 *   }
 * }
 */

userCtrl.login = function(req, res) {

  validationService.validateInput(req.body, 'login')
  .then(function() {

    userService.login(req.body)
    .then((token) => {
      // res.cookie('token', token, { 'maxAge': config.web.timeouts.session * 1000, 'httpOnly': true });
      res.ok({access_token: token});
    })
    .catch((err) => {
      if (typeof err == 'string') {
        res.badRequest(res.__(err));
      } else {
        res.badRequest(err);
      }
    });
  })  
  .catch(function(err){
    res.badRequest(err);
  });

  
};


/**
 * @api {get} /api/v1/users/me User object
 * @apiHeader {String} token
 * @apiName read
 * @apiGroup User
 * @apiDescription Gets the data of the user that is doing the request
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *   "gender": "Male",
 *   "email": "asdlkasldk@asdkjkda.com",
 *   "lastName": "asdads",
 *   "firstName": "asdads",
 *   "_id": "56a7c125706144ab0b2ab7e9",
 *   "updatedAt": "2016-01-26T18:55:33.800Z",
 *   "createdAt": "2016-01-26T18:55:33.800Z",
 *   "image": "http://simpleicon.com/wp-content/uploads/user1.png",
 *   "isActive": false,
 *   "isValidated": false,
 *   "role": "User"
 * }
 */
userCtrl.read = function(req, res) {
  if(req.user) req.user.image = config.cdn.url + req.user.image;
  res.ok({data: req.user});
};

module.exports = userCtrl;
