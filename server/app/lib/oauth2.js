'use strcit';

var oauth2orize = require('oauth2orize');
var login = require('connect-ensure-login');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user').User;
var Client = require('../models/client').Client;
var AuthCode = require('../models/authcode').AuthCode;
var AccessToken = require('../models/accesstoken').AccessToken;
var RefreshToken = require('../models/refreshtoken').RefreshToken;
var passport = require('./passport');
var trustedClientPolicy = require('../middlewares/isTrustedClient.js');
var userHandler = require('../middlewares/oauthUserhandler.js');
var config = require('../../config/appConfig');

module.exports = function(app) {
  // Create OAuth 2.0 server
  var server = oauth2orize.createServer();

  server.serializeClient(function(client, done) {
    return done(null, client._id);
  });

  server.deserializeClient(function(id, done) {
    Client.findOne({_id: id}, function(err, client) {
      if (err) {
        return done(err);
      }
      return done(null, client);
    });
  });

  // Generate authorization code
  server.grant(oauth2orize.grant.code(function(client, redirectURI, user, ares, done) {
    AuthCode.create({
      clientId: client.clientId,
      redirectURI: redirectURI,
      userId: user._id,
      scope: ares.scope
    }).done(function(err,code){
      if(err){return done(err,null);}
      return done(null,code.code);
    });
  }));

  // Generate access token for Implicit flow
  // Only access token is generated in this flow, no refresh token is issued
  server.grant(oauth2orize.grant.token(function(client, user, ares, done) {
    AccessToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
      if (err){
        return done(err);
      } else {
        AccessToken.create({ userId: user._id, clientId: client.clientId }, function(err, accessToken){
          if(err) {
            return done(err);
          } else {
            return done(null, accessToken.token);
          }
        });
      }
    });
  }));

  // Exchange email & password for access token.
  server.exchange(oauth2orize.exchange.password(function(client, email, password, scope, done) {
    User.findOne({ email: email})
    // .where({active:1})
    .exec(function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        //if (validateIsActive(user, done)) return;

        var pwdCompare = bcrypt.compareSync(password, user.password);
        if(!pwdCompare){ return done( null, false) };

        AccessToken.findOne({userId: user._id}, function(err, accessToken){
          if (err) {
            return done(err);
          } else{

            if(accessToken && (new Date(accessToken.createdAt.getTime() + 1000 * config.oauth.tokenLife) > new Date())){

              RefreshToken.findOne({ userId: user._id, clientId: client.clientId }, function(err, refreshToken) {
                if (err) {
                  return done(err);
                } else {
                  var expires = ((accessToken.createdAt.getTime() + 1000 * config.oauth.tokenLife) - new Date().getTime())/1000
                  done(null, accessToken.token, refreshToken.token, { 'expires_in': expires });
                }
              });

            } else{

              // Remove Refresh and Access tokens and create new ones
              RefreshToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
                  if (err) {
                    return done(err);
                  } else {
                    AccessToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
                      if (err){
                        return done(err);
                      } else {
                        RefreshToken.create({ userId: user._id, clientId: client.clientId }, function(err, refreshToken){
                          if(err){
                            return done(err);
                          } else {
                            AccessToken.create({ userId: user._id, clientId: client.clientId }, function(err, accessToken){
                              if(err) {
                                return done(err);
                              } else {
                                done(null, accessToken.token, refreshToken.token, { 'expires_in': config.oauth.tokenLife });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
              });

            }
          }
        });
      });
    }));

  // Exchange refreshToken for access token.
  server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {
      RefreshToken.findOne({ token: refreshToken }, function(err, token) {
          if (err) { return done(err); }
          if (!token) { return done(null, false); }
          if (!token) { return done(null, false); }

          User.findOne({_id: token.userId, isActive: true}, function(err, user) {

              if (err) { return done(err); }
              if (!user) { return done(null, false); }

              // Remove Refresh and Access tokens and create new ones
              RefreshToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
                if (err) {
                  return done(err);
                } else {
                  AccessToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
                    if (err){
                      return done(err);
                    } else {
                      RefreshToken.create({ userId: user._id, clientId: client.clientId }, function(err, refreshToken){
                        if(err){
                          return done(err);
                        } else {
                          AccessToken.create({ userId: user._id, clientId: client.clientId }, function(err, accessToken){
                            if(err) {
                              return done(err);
                            } else {
                              done(null, accessToken.token, refreshToken.token, { 'expires_in': config.oauth.tokenLife });
                            }
                          });
                        }
                      });
                    }
                  });
                }
             });
          });
      });
  }));

  
  // Exchange token from facebook for acces_token
  server.exchange(oauth2orize.exchange.clientCredentials(function(user, done) {
    Client.findOne({name: 'jinglz-ios'}, function (err, client) {
        if (err) {
          return done(err);
        }
        AccessToken.findOne({userId: user._id}, function(err, accessToken){
        if (err) {
          return done(err);
        }

        if(accessToken && (new Date(accessToken.createdAt.getTime() + 1000 * config.oauth.tokenLife) > new Date())){

          RefreshToken.findOne({ userId: user._id, clientId: client.clientId }, function(err, refreshToken) {
            if (err) {
              return done(err);
            } else {
              var expires = ((accessToken.createdAt.getTime() + 1000 * config.oauth.tokenLife) - new Date().getTime())/1000
              //if (validateIsActive(user, done)) return;
              done(null, accessToken.token, refreshToken.token, { 'expires_in': expires });
            }
          });

        } else {
          // Remove Refresh and Access tokens and create new ones
          RefreshToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
              if (err) {
                return done(err);
              } else {
                AccessToken.remove({ userId: user._id, clientId: client.clientId }, function (err) {
                  if (err){
                    return done(err);
                  } else {
                    RefreshToken.create({ userId: user._id, clientId: client.clientId }, function(err, refreshToken) {
                      if(err){
                        return done(err);
                      } else {
                        AccessToken.create({ userId: user._id, clientId: client.clientId }, function(err, accessToken) {
                          if(err) {
                            return done(err);
                          } else {
                            //if (validateIsActive(user, done)) return;
                            done(null, accessToken.token, refreshToken.token, { 'expires_in': config.oauth.tokenLife });
                          }
                        });
                      }
                    });
                  }
                });
              }
          });
        }
      });
    });
  }));

  function validateIsActive(user, done){
    var user = (user) ? user : '' ;
    var isActive = !user.isActive;
    if( isActive ) done(null, { message: 'User not activated', errorCode: 403 });
    return isActive;
  }

  // Initialize passport
  app.use(passport.initialize());
  app.use(passport.session());

  /***** OAuth authorize endPoints *****/

  app.route('/oauth/authorize').get(
    login.ensureLoggedIn(),
    server.authorize(function(clientId, redirectURI, done) {
      Client.findOne({clientId: clientId}, function(err, client) {
        if (err) { return done(err); }
        if (!client) { return done(null, false); }
        if (client.redirectURI != redirectURI) { return done(null, false); }
        return done(null, client, client.redirectURI);
      });
    }),
    server.errorHandler(),
    function(req, res) {
      res.render('dialog', {
        transactionID: req.oauth2.transactionID,
        user: req.user,
        client: req.oauth2.client
      });
    }
  );

  app.route('/oauth/authorize/decision').
    post(login.ensureLoggedIn(), server.decision());

  /***** OAuth token endPoint *****/
  app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });

  app.route('/oauth/token')
  .post(
    trustedClientPolicy,
    passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
    server.token(),
    server.errorHandler()
  );
};
