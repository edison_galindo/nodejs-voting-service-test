var config  = require('../../config/appConfig');
var fs = require('fs');
var swig = require('swig');
var cheerio = require('cheerio');
var mailgun = require('mailgun-js')({apiKey: config.mailgun.apiKey, domain: config.mailgun.domain });

module.exports = {
  send: function (template, to, params, cb) {
    // console.log('config====================>',config);
    var templateFile = __dirname + '/../mails/' + template + '.html';
    if (fs.existsSync(templateFile)) {
      var template = swig.compileFile(templateFile);
      params.host = config.host.front;
      var html = template(params);
      var $ = cheerio.load(html);

      var mail = {
        from: config.email.fromEmail,
        to: to,
        subject: params.subject,
        html: html
      };

      mailgun.messages().send(mail, function (err, res) {
        if (err) {
          console.log('Mail error: ', err, {to: to});
        }
        if (cb) cb(err, res);
      });
    } else {
      console.log('Mail template ' + templateFile + ' not found!');
    }
  }
};