'use strict';

var exports = module.exports;

exports.filterFind = function( filter, fields){
  var q, find, regex;
  find = {};

  if (filter){
    filter = JSON.parse(filter);
    q = filter.q
  } 
  if(q){
    regex = new RegExp(q, "i");
    var newFields = fields.map(function(field, i){
      var obj = {};
      obj[field] = regex;
      return obj;
    });
    find = { 
      $or: newFields
    }; 
    if( q.length >= 24 ) find.$or.push({_id: q});
  }
  return find;
}

exports.filterPaginate = function(filter){
  var paginate = {};
 
  paginate.skip = filter._page || 1;
  paginate.limit = filter._perPage || 30;

  return paginate;
}