'use strict';

/**
 * Simple library with utility functions
 *
 * @author Andrés Aldana <andres@rokk3rlabs.com>
 */

var fs = require('fs'),
    path = require('path'),
    _ = require('lodash');

module.exports = {
  extractEmails: extractEmails,
  formatVars: formatVars,
  addRecipients: addRecipients,
  // formatAttachments: formatAttachments
};

/**
 * This function check and formatted the array of recipients
 *
 * @param  {String|Object|Collection} emails Recipients to send email
 * @return {Collection} Formatted collection with the recipients
 */
function extractEmails(emails) {
  var extractedEmails = [];
  if(_.isArray(emails)) {
    if(_validateEmailArray(emails)) {
      extractedEmails = emails;
    } else {
      extractedEmails = _formatEmailArray(emails);
    }
  } else if(_.isString(emails)) {
    extractedEmails = _formatEmailArray([emails]);
  }

  return extractedEmails;
}

/**
 * This function check and formatted the array of variables
 *
 * @param  {Object|Collection} vars Variables to send at the template
 * @return {Collection} Formatted vars
 */
function formatVars(vars) {
  var formattedVars = [];
  if(!_.isEmpty(vars)) {
    if(_.isArray(vars)) {
      formattedVars = vars;
    } else if(_.isPlainObject(vars)) {
      formattedVars.push(vars);
    }
  }

  return formattedVars;
}

/**
 * It add the recipients at the array of variables,
 * it uses when the user pass the merge vars to the template
 *
 * @param {Object|Collection} vars variables to send
 * @param {Collection} Formatted variables
 */
function addRecipients(vars, emails) {
  var formattedVars = [];
  if(!_.isEmpty(vars) && !_.isEmpty(emails)) {
    for(var i in emails) {
      formattedVars.push({
        rcpt: emails[i],
        vars: vars
      });
    }
  }

  return formattedVars;
}

/**
 * This function check and formatted at the attachments
 *
 * @param  {Object|Colletion} attachments Attachments
 * @return {Collection} Formatted attachments
 */
function formatAttachments(attachments) {
  var formattedAttachments = [];
  if(!_.isEmpty(attachments)) {
    if(_.isArray(attachments)) {
      if(_validateAttachmentsArray(attachments)) {
        formattedAttachments = attachments;
      } else {
        for(var i in attachments) {
          formattedAttachments.push(_getFileStructure(attachments[i]));
        }
      }
    } else if(_.isPlainObject(attachments)) {
      if(_validateAttachmentsArray([attachments])) {
        formattedAttachments.push(attachments);
      } else {
        formattedAttachments.push(_getFileStructure(attachments));
      }
    }
  }

  return formattedAttachments;
}

/*************************** Private functions ****************************************/

/**
 * This function validates the structure of emails array
 *
 * @param  {Array|Collection} emails emails array
 * @return {Boolean} if it's correct returns true otherwise false
 */
function _validateEmailArray(emails) {
  for(var i in emails) {
    if(!emails[i].hasOwnProperty('name') ||
       !emails[i].hasOwnProperty('type') ||
       !emails[i].hasOwnProperty('email')) {
      return false;
    }
  }

  return true;
}

/**
 * This function validates the structure of attachments array
 *
 * @param  {Array|Collection} attachments Attachments array
 * @return {Boolean} if it's correct returns true otherwise false
 */
function _validateAttachmentsArray(attachments) {
  for(var i in attachments) {
    if(!attachments[i].hasOwnProperty('type') ||
       !attachments[i].hasOwnProperty('name') ||
       !attachments[i].hasOwnProperty('content')) {
      return false;
    }
  }

  return true;
}

/**
 * This function gives the correct structure at the emails array
 *
 * @param  {Array} emails Emails array
 * @return {Array|Collection} Formatted array
 */
function _formatEmailArray(emails) {
  var formattedEmails = [];
  for(var i in emails) {
    if(_.isString(emails[i])) {
      formattedEmails.push({
        name: '',
        email: emails[i],
        //default type is 'to'
        type: 'to'
      });
    }
  }

  return formattedEmails;
}

/**
 * This function returns the correct structure of the files
 *
 * @param  {Object} params
 * @param  {String} params.name Optional
 * @param  {String} params.filePath Required
 * @return {Object} Formatted object
 */
function _getFileStructure(params) {
  if(fs.existsSync(params.filePath)) {
    var fileExt = path.extname(params.filePath),
        fileName = path.basename(params.filePath, fileExt),
        content;

    try {
      content = fs.readFileSync(params.filePath, 'base64');
    } catch (e) {
      throw e;
    }

    return {
      name: params.name || fileName,
      type: _getMimeType(fileExt),
      content: content.toString()
    };
  }

  throw new Error('File ' +params.filePath+ ' not found');
}

/**
 * This function returns the mime type of the files.
 * These mime types are in mimeTypes.json file
 *
 * @param  {String} extension Extension of file
 * @return {String} Mime type
 */
function _getMimeType(extension) {
  var type = mimeTypes.txt;
  if(!_.isEmpty(extension)) {
    extension = extension.replace(/\./g, '').toLowerCase();
    type = _.isUndefined(mimeTypes[extension]) ? mimeTypes.txt : mimeTypes[extension];
  }

  return type;
}
