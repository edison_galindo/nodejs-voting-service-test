'use strict';

/**
 * This is a simple hook that improves the functionality of the actual service.
 * The hooks is the correct form for extending sails.
 * Please see this documentation https://github.com/balderdashy/sails-docs/tree/master/concepts/extending-sails
 *
 * @author Andrés Aldana <andres@rokk3rlabs.com>
 * @version 0.0.1
 */

var mandrill = require('mandrill-api/mandrill'),
    util = require('./util'),
    _ = require('lodash');

module.exports = function (credentials) {

  /**
   * It's the basic structure for email message (administrable),
   * please for more information you can see this https://mandrillapp.com/api/docs/messages.JSON.html#method=send
   *
   * @type {Object}
   */
  var messageTemplate = require('./message.json');

  /**
   * Is the mandrill instance with the api key that was merged with the appConfig params
   *
   * @type {Object}
   */
  var client = (function () {
    messageTemplate.from_email = credentials.fromEmail;
    messageTemplate.from_name = credentials.fromName;
    return new mandrill.Mandrill(credentials.apiKey);
  })();

  /**
   * Are the defaults options, please see https://mandrillapp.com/api/docs/messages.JSON.html
   *
   * @type {Object} options
   * @type {Boolean} async
   * @type {String} ipPool
   * @type {String} sendAt
   */
  var options = {
    async: true,
    ipPool: '',
    sendAt: ''
  };

  /**
   * Is the only method that the user can call it, and my hook will make other things automatically
   * Please for more information you can see the hook's readme file
   *
   * @param  {Object} params All options for send an email
   * @param  {String|Array|collection} to Recipients (required)
   * @param  {String} templateName Name of template in mandrill account (optional)
   * @param  {String} subject Subject email (optional)
   * @param  {String} html When we not need templates can use the html string (optional)
   * @param  {String} text When we not need templates can use the text string (optional)
   * @param  {String} replyTo Email to reply (optional)
   * @param  {Boolean} preserveRecipients whether or not to expose all recipients in to "To" header for each email.
   *                                      Default value is false (optional)
   * @param  {String} mergeLanguage the merge tag language to use when evaluating merge tags,
   *                                either mailchimp or handlebars.
   *                                Default value is mailchimp (optional)
   * @param  {Array|Object} globalVars Global merge variables to use for all recipients.
   *                             You can override these per recipient with the next parameter. (optional)
   * @param  {Array|Object} vars Per-recipient merge variables, which override global merge
   *                             variables with the same name. (optional)
   * @param  {Array|Object} attachments Supported attachments to add to the message. (optional)
   *
   * @param  {Function} cb
   */
  function send(params, cb) {
    if(_.isEmpty(params)) {
      return cb('The parameters mustn\'t be empty');
    }

    if(params.templateName) {
      _sendTemplate(params, cb);
    } else {
      _sendTextOrHtml(params, cb);
    }
  }

/*************************** Private functions ****************************************/

  /**
   * Send the email with template, so it use same parameter of send function
   *
   * @param  {Object}   paramsToSend
   * @param  {Function} cb
   */
  function _sendTemplate (paramsToSend, cb) {
    paramsToSend.text = '';
    paramsToSend.html = '';
    client.messages.sendTemplate({
      'template_name': paramsToSend.templateName,
      'template_content': [],
      'message': _buildMessage(paramsToSend),
      'async': options.async,
      'ip_pool': options.ipPool,
      'send_at': options.sendAt
    }, function(result) {
      //Message sent success
      cb(null, result);
    }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      cb('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
  }

  /**
   * Send the email with text or html content, so it use same parameter of send function
   *
   * @param  {Object}   paramsToSend
   * @param  {Function} cb
   */
  function _sendTextOrHtml (paramsToSend, cb) {
    paramsToSend.mergeLanguage = '';
    paramsToSend.globalVars = [];
    paramsToSend.vars = [];
    client.messages.send({
      'message': _buildMessage(paramsToSend),
      'async': options.async,
      'ip_pool': options.ipPool,
      'send_at': options.sendAt
    }, function(result) {
      //Message sent success
      cb(null, result);
    }, function(e) {
      // Mandrill returns the error as an object with name and message keys
      cb('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
  }

  /**
   * Build the structure of message for each email send
   *
   * @param  {Object} paramsToSend
   * @return {Object}
   */
  function _buildMessage(paramsToSend) {
    //Clone default structure
    var message = _.clone(messageTemplate);
    //Building the message with passed parameters
    message.to = util.extractEmails(paramsToSend.to);

    message.subject = paramsToSend.subject || message.subject;
    message.preserve_recipients = _.isUndefined(paramsToSend.preserveRecipients) ?
                                  message.preserve_recipients :  paramsToSend.preserveRecipients;
    message.merge_language = paramsToSend.mergeLanguage || message.merge_language;
    message.headers['Reply-To'] = paramsToSend.replyTo || message.headers['Reply-To'];
    message.global_merge_vars = util.formatVars(paramsToSend.globalVars);
    message.merge_vars = util.formatVars(paramsToSend.vars);
    message.merge_vars = util.addRecipients(message.merge_vars, _.map(message.to, 'email'));
    message.attachments = [];

    if(paramsToSend.fromEmail){
      message.from_email = paramsToSend.fromEmail;
    }

    if(paramsToSend.fromName){
      message.from_name = paramsToSend.fromName;
    }

    if(!_.isEmpty(paramsToSend.text)) {
      message.text = paramsToSend.text;
      message.merge = false;
    } else if(!_.isEmpty(paramsToSend.html)) {
      message.html = paramsToSend.html;
      message.merge = false;
    }

    return message;
  }

  return {
    send: send,
    options: options
  };
};
