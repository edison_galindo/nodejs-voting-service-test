var _               = require('lodash');
var async           = require('async');
var util            = require('util');
var jwt             = require('jsonwebtoken');
var bcrypt          = require('bcrypt-nodejs');
var moment          = require('moment');
var errorRepo       = require('../lib/error-repo');
var User            = require('../models/user').User;
var Code            = require('../models/code').Code;
var config          = require('../../config/appConfig');
var utils           = require('../services/StringService');
var userService      = {};

/*
 * [CRUD Handler Functions]
 */

userService.createUser = (data, res) => {

  return new Promise((resolve, reject) => {

    /* Persists data user in db */
    let newUser = (cb) => {
      data.email = data.email.toLowerCase();
      data.role = 'user';
      User.create(data, cb);
    }

    let getToken = (user, cb) => {
      token = userService.generateToken(data.email, config.web.timeouts.session);
      cb(null, user, token);
    }

    let setResponse = (user, token, cb) => {
      let response = {
        userId: user._id,
        email: user.email,
        token: token
      }

      cb(null, response);
    }

    async.waterfall([
      newUser,
      getToken,
      setResponse
    ], function (err, result) {
      if (err) {
        return reject(err);
      }
      resolve(result);
    });
  });

};

userService.changePassword = function(currentPassword, newPassword, user) {
  return new Promise(function(resolve, reject) {
    if(bcrypt.compareSync(currentPassword, user.password)){
      User.findByIdAndUpdate(user.id, { $set: { password:  bcrypt.hashSync(newPassword)}}, function (err, updatedUser) {
        if (err) return reject(err);
        return resolve(updatedUser);
      });
    } else {
      reject('err');
    }
  });
};

/**
 * Login to get a JWT token
 * @param  {[String]} email [email]
 * @param  {[type]} res    [description]
 * @return {[type]}        [description]
 */
userService.login = function(params) {

  return new Promise(function(resolve, reject) {

    User.findOne({email: params.email})
    .exec(function(err, fetchedUser){
      if(err) reject(err);
      if(fetchedUser == null) return reject('user_not_found');
      if (bcrypt.compareSync(params.password, fetchedUser.password)) {
        // Credentials are correct; Create Session and Token to be used
        var token = userService.generateToken(fetchedUser.email, config.web.timeouts.session);
        // Save Token
        // 
        fetchedUser.lastActivityDate = new Date();
        
        fetchedUser.save();
        resolve(token)
      } else {
        reject('fail password');
      }
    });
  });
};


userService.generateToken = function(data, timeout) {
  timeout = timeout || config.web.timeouts.session;
  var secret = config.web.tokenSecret;
  var opts = { 'expiresIn': timeout };
  if(typeof data === 'string') data = { 'email': data };
  // Generate token with given data
  return jwt.sign(data, secret, opts);
};

userService.verifyToken = function(token) {
  return new Promise(function(resolve, reject) {
    if(!!token) {

      // Verify the token by using ssecret and email
      jwt.verify(token, config['tokenSecret'], function(err, decoded) {
        if(!!err) {
          console.log(err);
          err['errorCode'] = 401;
          reject(errorRepo.prepare(new Error(), 'ERR_AUTHFAILURE', [], err));  // Expired token and Invalid token can be distinguished here
        } else {
          if(!!decoded['password']) resolve({ 'email': decoded['email'], 'password': decoded['password'] });
          else resolve(decoded['email']);
        }
      });
    } else {
      // Token or Email not specified
      reject(errorRepo.prepare(new Error(), 'ERR_AUTHFAILURE', [], { 'errorCode': 401 }));
    }
  });
};

userService.refreshToken = function(req) {
  var newToken = userService.generateToken(req.user.email, config.web.timeouts['session']);
  var userOptions = {
    'criteria': { 'id': req.user.id },
    'data': { 'token': newToken }
  };
  // return crudWorker.do('update', [User, userOptions]);
};

module.exports = userService;
