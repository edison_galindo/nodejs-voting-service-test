const util             = require('util');
const _                = require('lodash');
const async            = require('async');
const Promise          = require('bluebird');
const i18n             = require('i18n');
const errorRepo        = require('../../lib/error-repo');

const ValidationService = {};

ValidationService.formatDefinitions = {};
ValidationService.formatDefinitions['float'] = {
  'regex': /^[-]?(\d+)(\.\d+)?$/,
  'failure-message': '"%s" ' + i18n.__("is_not_number")
};
ValidationService.formatDefinitions['unsignedfloat'] = {
  'regex': /^[-0-9]+(?:\.[0-9]{1,2})?$/,
  'failure-message': '"%s" ' + i18n.__("is_not_number")
};
ValidationService.formatDefinitions['integer'] = {
  'regex': /^\d+$/,
  'failure-message': '"%s" ' + i18n.__("is_not_integer")
};
ValidationService.formatDefinitions['letters'] = {
  'regex': /^[A-Za-z]+$/,
  'failure-message': '"%s" ' + i18n.__("is_not_only_letters_string")
};
ValidationService.formatDefinitions['name'] = {
  'regex': /^[A-Za-z0-9\_\-&\. ]+$/,
  'failure-message': '"%s" ' + i18n.__("is_not_name")
};
ValidationService.formatDefinitions['date'] = {
  'regex': /^\d{4}\-\d{2}\-\d{2}$/,
  'failure-message': '"%s" ' + i18n.__("is_not_date")
};
ValidationService.formatDefinitions['utc-date'] = {
  'regex': /^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/,
  'failure-message': '"%s"  ' + i18n.__("is_not_date_utc")
};
ValidationService.formatDefinitions['date-timeZone'] = {
  'regex': /^\d{4}\-\d{2}\-\d{2} [+-\s]?\d{2}\:\d{2}$/,
  'failure-message': '"%s" ' + i18n.__("is_not_date")
};
ValidationService.formatDefinitions['email'] = {
  'regex': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  'failure-message': '"%s" ' + i18n.__("is_not_mail")
};
ValidationService.formatDefinitions['password'] = {
  'regex': /^(?=.*\d)(?=.*[a-zA-Z])(?!.*\s).{0,}$/,
  'failure-message': '"%s" ' + i18n.__("is_not_password")
};
ValidationService.formatDefinitions['uuid'] = {
  'regex': /^[a-f\d]{24}$/i,
  'failure-message': '"%s" ' + i18n.__("is_not_uuid")
};
ValidationService.formatDefinitions['phone'] = {
  'regex': /^[+]?(\d+)$/,
  'failure-message': '"%s" ' + i18n.__("is_not_phone")
};
ValidationService.formatDefinitions['boolean'] = {
  'regex': /^(true|false|1|0)$/,
  'failure-message': '"%s" ' + i18n.__("is_not_boolean")
};
ValidationService.formatDefinitions['marriage-status'] = {
  'regex': /^(Married)|(Single)|(Divorced)|(Widowed)|(Other)$/,
  'failure-message': '"%s" ' + i18n.__("is_not_a_valid_marriage_status")
};
ValidationService.formatDefinitions['vote-type'] = {
  'regex': /^(up)|(down)$/,
  'failure-message': '"%s" ' + i18n.__("vote_type")
};

ValidationService.formats = {};
ValidationService.formats['createUser'] = {
  'email':            ['required', 'email'],
  'password':         ['required', 'min-length-8', 'password'],
  'age':              ['integer'],
  'marriage_status':  ['marriage-status']
};
ValidationService.formats['createPoll'] = {
  'title':            ['required'],
  'description':      ['required']
};
ValidationService.formats['updatePoll'] = {
  'title':            ['name'],
  'description':      []
};
ValidationService.formats['readPoll'] = {
  'id': ['required', 'uuid']
};
ValidationService.formats['vote'] = {
  'id': ['required', 'uuid'],
  'type': ['required', 'vote-type']
};
ValidationService.formats['checkVote'] = {
  'id': ['required', 'uuid']
};
ValidationService.formats['updateUser'] = {
  'email':        ['email'],
  'password':     ['min-length-8', 'password'],
  'first_name':   ['name', 'max-length-50'],
  'last_name':    ['name', 'max-length-50'],
  'role':         ['role'],
  'status':       ['status'],
  'country':      ['max-length-200'],
  'state':        ['max-length-200'],
  'age':          ['integer'],
  'picture':      []
};
ValidationService.formats['requestResetPassword'] = {
  'email':    ['required', 'email']
};
ValidationService.formats['resetPassword'] = {
  'email':    ['required', 'email'],
  'token':    ['required'],
  'password': ['required', 'min-length-8', 'password']
};
ValidationService.formats['login'] = {
  'email':    ['required', 'email'],
  'password': ['required', 'min-length-8', 'password']
};

ValidationService.validateFieldFormats = function(body, formatSpec) {
  return new Promise(function(resolve, reject) {
    var specifiedFields = Object.keys(formatSpec);
    var passedFields    = Object.keys(body);
    var extraFields     = _.difference(passedFields, specifiedFields);

    if(extraFields.length > 0) {
      var responseData = {
        'errorCode': 400,
        'customMessage': 'Fields "' + extraFields.join(', ') + '" are not expected'
      };
      reject(errorRepo.prepare(new Error(), 'ERR_VALIDATION', null, responseData));
    }

    for(var field in formatSpec) {
      var value           = String(body[field] || '');
      var validationNames = formatSpec[field] || [];
      if(typeof validationNames === 'string') validationNames = [validationNames];

      for(var i=0; i<validationNames.length; i++) {
        var validationName = validationNames[i];
        var validator      = ValidationService.formatDefinitions[validationName];

        if(value.length > 0) {
          if(validationName.startsWith('max-length-')) {
            var maxLength = parseInt(validationName.substring('max-length-'.length));
            if(value.length > maxLength) {
              var responseData = {
                'errorCode': 412,
                'customMessage': '\"' + field + '\" is longer than ' + maxLength + ' characters'
              };
              reject(errorRepo.prepare(new Error(), 'ERR_VALIDATION', null, responseData));
            }
          } else if(validationName.startsWith('min-length-')) {
            var minLength = parseInt(validationName.substring('min-length-'.length));
            if(value.length < minLength) {
              var responseData = {
                'errorCode': 412,
                'customMessage': '\"' + field + '\" is not a minimum of ' + minLength + ' characters'
              };
              reject(errorRepo.prepare(new Error(), 'ERR_VALIDATION', null, responseData));
            }
          } else if(!!validator && !validator['regex'].test(value)) {
            var responseData = {
              'errorCode': 412,
              'customMessage': util.format(validator['failure-message'], field)
            };
            reject(errorRepo.prepare(new Error(), 'ERR_VALIDATION', field, responseData));
          }
        } else if(validationName === 'required') {
          var responseData = {
            'errorCode': 412,
            'customMessage': field + ' is not included'
          };
          reject(errorRepo.prepare(new Error(), 'ERR_VALIDATION', null, responseData));
        }
      }
    }
    resolve();
  });
};

ValidationService.validateInput = function(data, formatSpecName) {
  return new Promise(function(resolve, reject) {
    var formatSpec = ValidationService['formats'][formatSpecName];
    ValidationService.validateFieldFormats(data, formatSpec)
    .then(function() {
      resolve();
    })
    .catch(reject);
  });
};

module.exports = ValidationService;
