var config          = require('../../config/appConfig');
var _               = require('lodash');
var jwt             = require('jsonwebtoken');
var request         = require('request');
var q               = require('q');
var crypto          = require('crypto');
var qs              = require('querystring');
var i18n            = require('i18n');
var oauth           = require("oauth");
var exports         = module.exports;
var errObject       = {
                      code:401,
                      userMessage:"",
                      serverInfo: "",
                      data:{}
                    }


/**
 * Generates jwt token
 * @param  {Object}  user User instance
 * @param  {Function} cb   Optional function to return the token on a callback
 * @return String        Return the jwt token
 */
exports.generateToken = function(user, cb){
    var payload = { "userId": user._id, 'expiresIn': config.jwt['tokenLife']};
    var secret = config.authentication['tokenSecret'];
    // Generate token with given data
    var token = jwt.sign( payload, secret );
    //Add compatibility for oauth module
    if(cb && typeof cb == 'function'){
      cb(null, token);
    }
    return token;
};


/**
 * Verify the presence and validity of jwt token
 * @param  {Object}   req      Express Request Object
 * @param  {Function} callback Callback function
 */
exports.verifyAuthToken = function(req, callback){
    
  if(req.headers.authorization){
      
    var authInfo = req.headers.authorization.split(' ')
    
    if(authInfo.length == 2){
        
        var token = _.trim(authInfo[1]);
        
        jwt.verify(token, config.authentication.tokenSecret, callback);

    }else{
        callback( { error:'invalid-token-info' } )
    }

  }else{

      callback( { error:'no-token-present' } )

  }

};

exports.getRequestToken = function(params){
  
    var defer = q.defer();
    errObject.serverInfo = "jwt_authService_getRequestToken"


    // Step 1. Obtain request token for the authorization popup.
    request.post(params, function(err, response, body) {
      

      if(err){

        errObject.data = err
        return defer.reject(errObject);
      }

      if(response.statusCode == '500'){

        errObject.code = 500
        errObject.data = response
        return defer.reject(errObject);
      }else if( response.statusCode == '401'){  

        var body = response.body
        var json = JSON.parse(body);

        err = json.errors.length > 0 ? json.errors[0] : json.errors;

        errObject.code = err.code
        errObject.userMessage = err.message
        errObject.data = params.url

        return defer.reject(errObject);
      }
      
      var oauthToken = qs.parse(body);
      

      if(oauthToken){
        log('oauthToken:', oauthToken)
        return defer.resolve(oauthToken);
      }
      
      return defer.reject({error:'invalid-request'});

    });

    return defer.promise;
}

/**
 * Exchange authentication code for access token
 * @param  {String} url    URL of the service to get access token
 * @param  {Object} params Parameters required to service to get access token
 * @return {Q.Promise}     Q.promise
 */
exports.exchangeCodeForToken = function(params) {
  var defer = q.defer();
  errObject.serverInfo = "jwt_authService_exchangeCodeForToken"

  request.post(params, function(err, response, token) {
    
    if(err){
      errObject.data = err
      return defer.reject(errObject);
    }
    
    if(response.statusCode == '500'){
      errObject.code = 500
      errObject.data = response
      return defer.reject(response);
    }
    
    if(token){
      return defer.resolve(token);
    }
    
    errObject.userMessage = i18n.__("invalid_request")
    return defer.reject(errObject);

  });

  return defer.promise;
};
