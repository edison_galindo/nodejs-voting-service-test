/*!
 * ZEMOGA Poll API
 * Author(c) 2018 Edison Galindo <edison.galindo@gmail.com>
 */

const _       = require('lodash');
const async   = require('async');
const moment  = require('moment');
const config  = require('../../config/appConfig');
const Poll    = require('../models/poll').Poll;
const PollVote = require('../models/pollVote').PollVote;
const ObjectID            = require('mongodb').ObjectID

const PollService = {};

PollService.create = (data) => {
	
	return new Promise((resolve, reject) => {

		Poll.create(data, function(err, poll){
			
			if(err){
				reject(err);
			}
			resolve(poll);
		});
		
	});	

}

PollService.list = (queryParams) => {

	let pagination = {};
  pagination.page = (queryParams.page) ? parseInt(queryParams.page) : 1;
  pagination.limit = (queryParams.limit) ? parseInt(queryParams.limit) : 10;

	return new Promise((resolve, reject) => {

		Poll.paginate({}, pagination, function(err, pollItems){
			
			if(err){
				reject(err);
			}
			resolve(pollItems);
		});
		
	});	

}

PollService.listWithVotes = (queryParams, userId) => {

	console.log('queryParams.user._id', userId);

	let user = userId;

	let pagination = {};
  pagination.page = (queryParams.page) ? parseInt(queryParams.page) : 1;
  pagination.limit = (queryParams.limit) ? parseInt(queryParams.limit) : 10;

  return new Promise((resolve, reject) => {

  	Poll.aggregate(
  	[
      {
        $lookup:
          {
              from:"pollvotes",
              localField:"_id",
              foreignField:"pollId",
              as:"votes"
          }
       	},
       	{
          $project: {
            _id: 0,
            id: "$_id",
            title: "$title",
            description: "$description",
            upTotal: "$upTotal",
            downTotal: "$downTotal",
            totalVotes: { $size: "$votes" },
            myVotes: {
          	 	$size:{ 
              	$filter : {
                  input: "$votes",
                  as : "vote",
                  cond : {
                      $eq: ["$$vote.userId",userId]
                  }
              	}
          	 	}
          	}
	        }
        }
    ], function (err, result) {
        if (err) {
            reject(err);
        } else {
           resolve(result);
        }
    });

	});	

}

PollService.read = (id) => {

	return new Promise((resolve, reject) => {

		Poll.findOne({_id: id})
		.lean()
		.exec(function(err, pollItems){
			
			if(err){
				reject(err);
			}
			resolve(pollItems);
		});
		
	});	

}

PollService.check = (pollId, userId) => {

	return new Promise((resolve, reject) => {

		PollVote.count({
			userId: new ObjectID(userId), 
			pollId: new ObjectID(pollId)
		})
		.exec(function(err, votes){
			
			if(err){
				reject(err);
			}
			if(votes < config.poll.maxVotesPerUser){
				resolve(true);
			}
			resolve(false);
			
		});
		
	});	

}

PollService.vote = (pollId, type, userId) => {

	return new Promise((resolve, reject) => {

		let query = {};

		let isAbleToVote = (cb) => {
			PollService.check(pollId, userId)
				.then(function(result){
					if(!result) {
						resolve(false);
					} else {
						cb();
					}
				})
				.catch((err) => {
				})
		} 

    let voteSave = (cb) => {
      PollVote.create({userId: userId, pollId: pollId, type: type}, cb);
    }

    let updatePollItem = (pollVote, cb) => {
    	if(type === 'up') {
    		query = { $inc: { upTotal: 1 } }
    	} else {
    		query = { $inc: { downTotal: 1} }
    	}
      Poll.update({_id: pollId}, query, cb);
    }

    async.waterfall([
    	isAbleToVote,
      voteSave,
      updatePollItem
    ], function (err, result) {
      if (err) {
        return reject(err);
      }
      resolve(result);
    });
		
	});	

}

module.exports = PollService;
