var paginationWorker = {};

/**
 * Customize the pagination response as
 *  {
 *    data: [],
 *    pagination {}
 *  }
 * @param  {Object} data
 * @return {Object} Formatted respinse
 */
paginationWorker.customize = function (data) {
  var response =  {};
  response.data = data.docs || [];
  response.pagination = data;
  delete response.pagination.docs;
  return response;
};

module.exports = paginationWorker;
