/*!
 * ZEMOGA Poll API
 * Author(c) 2016 Edison Galindo <edison.galindo@gmail.com>
 */
var mongoose = require('../init/db').mongoose;
var mongoosePaginate = require('mongoose-paginate');
var autoIncrement = require('mongoose-auto-increment');
var findOrCreate = require('mongoose-findorcreate')
var Schema = mongoose.Schema;
// var User = require('./user').User;
// 
autoIncrement.initialize(mongoose);

// create a schema
var PollSchema = new Schema({
  cursor: {
    type: String
  },
  title: { 
    type: String, 
    required: true
  },
  description: { 
    type: String, 
    required: true 
  },
  upTotal: { 
    type: Number, 
    validate: {
      validator: Number.isInteger, 
      message : '{VALUE} is not an integer value' 
    },
    default: 0 
  },
  downTotal: { 
    type: Number, 
    validate: {
      validator: Number.isInteger, 
      message : '{VALUE} is not an integer value' 
    },
    default: 0 
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

PollSchema.plugin(mongoosePaginate);
PollSchema.plugin(autoIncrement.plugin, { model: 'Poll', field: 'cursor' });
PollSchema.plugin(findOrCreate);
PollSchema.pre('update', function(cb) {
  this.update({},{ $set: { updatedAt: new Date() } });
  cb();
});

module.exports.Poll = mongoose.model('Poll', PollSchema);
