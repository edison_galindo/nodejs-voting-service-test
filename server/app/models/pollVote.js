/*!
 * ZEMOGA Poll API
 * Author(c) 2016 Edison Galindo <edison.galindo@gmail.com>
 */
const mongoose = require('../init/db').mongoose;
const mongoosePaginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;
// const User = require('./user').User;
// const Poll = require('./poll').Poll;

autoIncrement.initialize(mongoose);

// create a schema
const PollVoteSchema = new Schema({
  cursor: {
    type: String
  },
  userId: {
    type:mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required:true,
    index: true
  },
  pollId: {
    type:mongoose.Schema.Types.ObjectId,
    ref: 'Poll',
    required:true,
    index: true
  },
  type:{
    type: String, enum: ['up', 'down'],
    required: true
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

PollVoteSchema.plugin(mongoosePaginate);
PollVoteSchema.plugin(autoIncrement.plugin, { model: 'PollVote', field: 'cursor' });
PollVoteSchema.pre('update', function(cb) {
  this.update({},{ $set: { updatedAt: new Date() } });
  cb();
});

module.exports.PollVote = mongoose.model('PollVote', PollVoteSchema);
