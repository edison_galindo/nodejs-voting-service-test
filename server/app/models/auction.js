var mongoose = require('../init/db').mongoose;
var Schema = mongoose.Schema;

// create a schema
var AuctionSchema = new Schema({
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  isOpen: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

AuctionSchema.pre('update', function(cb) {
  this.update({},{ $set: { updatedAt: new Date() } });
  cb();
});

module.exports.Auction = mongoose.model('Auction', AuctionSchema);
