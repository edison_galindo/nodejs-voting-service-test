/**
 * Dependencies
 * @type {reqiure}
 */
var mongoose = require('../init/db').mongoose;
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var config = require('../../config/appConfig');
var findOrCreate = require('mongoose-findorcreate');
var mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');


// create a schema
var userSchema = new Schema({
  email: { type: String, required: true, match: /^([\w-\.\+]+@([\w-]+\.)+[\w-]{2,4})?$/ },
  password: { type: String, required: true, minlength: 8 },
  age: { type: Number, trim: true },
  marriage_status: { type: String, enum: ['Married', 'Single' , 'Divorced', 'Widowed', 'Other'] },
  firstName: { type: String, trim: true },
  lastName: { type: String, trim: true },
  role: { type: String, enum: ['admin', 'user'], default: 'user'},
  isActive: { type: Boolean, default: false },
  image: { type: String, default: null },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  lastActivityDate: { type: Date, default: Date.now }
});

userSchema.pre('save', function (next, promise, done) {
  if (!this.isNew) return next();
  var self = this;
  this.password = bcrypt.hashSync(this.password);
  if (!_.isEmpty(this.email)) {
    this.model('User').findOne({email: this.email}, function (err, user) {
      //var userExists = { errors: [{code: 'UM-409', message: 'user_exists'}]};
      if (err || user) {
        if (done) {
          return done(err || 'user_exists');
        } else {
          return promise(err || 'user_exists');
        }
      }
      next();
    });
  } else {
    next();
  }
});


userSchema.methods.setPassword = function () {
  this.password = bcrypt.hashSync(this.password);
};

userSchema.methods.fullName = function () {
  return this.firstName + ' ' + this.lastName;
};

userSchema.plugin(findOrCreate);
userSchema.plugin(mongoosePaginate);

module.exports.User = mongoose.model('User', userSchema);
