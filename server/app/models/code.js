var async = require('async');
var mongoose = require('../init/db').mongoose;
var Schema = mongoose.Schema;
var strWorker = require('../services/StringService');

// create a schema
var CodeSchema = new Schema({
  code: { type: String, minlength: 5 },
  userId: { type: String },
  isActive: { type: Boolean, default: true }
});

module.exports.Code = mongoose.model('Code', CodeSchema);
