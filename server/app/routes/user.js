var userCtrl = require('../controllers/UserController');
var isLoggedIn = require('../middlewares/isLoggedIn');

/*
 * Registers users routs in app
 */
module.exports = function(app) {

  /*
   * CRUD Routes
   */
  app.route('/api/v1/users/login')
    .post(userCtrl.login);

  app.route('/api/v1/users/me')
    .get(isLoggedIn, userCtrl.read);

  app.route('/api/v1/users')
    .post(userCtrl.create);

};
