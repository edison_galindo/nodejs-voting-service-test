var PollController = require('../controllers/PollController');
var isLoggedIn = require('../middlewares/isLoggedIn');

/*
 * Registers users routs in app
 */
module.exports = function(app) {

  /*
   * CRUD Routes
   */

  app.route('/api/v1/poll')
    .get(isLoggedIn, PollController.list);

  app.route('/api/v1/poll/my-votes')
    .get(isLoggedIn, PollController.listWithVotes);
    
  app.route('/api/v1/poll/:id')
    .get(isLoggedIn, PollController.read);
    
  app.route('/api/v1/poll')
    .post(isLoggedIn, PollController.create);

  app.route('/api/v1/poll/:id')
    .put(isLoggedIn, PollController.update);

  app.route('/api/v1/poll/vote')
    .post(isLoggedIn, PollController.vote);

  app.route('/api/v1/poll/:id/check')
    .get(isLoggedIn, PollController.check);

};
