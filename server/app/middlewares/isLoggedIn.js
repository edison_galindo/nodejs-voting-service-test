var async  = require('async');
var _      = require('lodash');
var jwt    = require('jsonwebtoken');
var User   = require('../models/user').User;
var config = require('../../config/appConfig');
var policyUtil   = require('./policyUtil');

module.exports = function(req, res, next) {

  async.waterfall([
    function validateToken(cb) {

      var token = policyUtil.extractToken(req);
      if(!token) return res.status(401).send('Unauthorized');

      jwt.verify(token, config.web.tokenSecret, function(err, decoded) {
        // console.log('verified token', err, decoded);
        if(!err) {
          cb(null, decoded);
        } else {
          cb(err);  // Expired token and Invalid token can be distinguished here
        }
      });
        
    },
    function getUser(decoded, cb) {
      User.findOne({email: decoded.email}, function(err, user){
        // console.log('User on isLoggedIn :::::::::::::::::', err, user);
        if(err) cb(err);
        if(!user) cb('user_not_found');
        req.user = _.omit(user, 'password');
        cb();
      });
    }
  ], function (err, result) {
      if(!err) next();
      else res.status(401).send('failed_login')
  });
    
};
