var policyUtil = {};
policyUtil.extractToken = function(req) {
  return (
    (!!req.params && !!req.params['token'])
    ? req.params['token']
    : (!!req.query && !!req.query['token'])
    ? req.query['token']
    : (!!req.body && !!req.body['token'])
    ? req.body['token']
    : (!!req.headers && !!req.headers['token'])
    ? req.headers['token']
    : null
  );
};

module.exports = policyUtil;