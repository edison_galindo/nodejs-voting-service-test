const async = require('async');
const _ = require('lodash');
const config = require('../../config/appConfig');
const Poll    = require('../models/poll').Poll;

const createPoll1 = function(cb){
  let query = _.pick(config.bootstrap.defaultPoll1);
  Poll.findOrCreate(config.bootstrap.defaultPoll1,config.bootstrap.defaultPoll1, function(err, poll, created) {
    console.log("Created poll:", created);
    cb(err);
  });
};

const createPoll2 = function(cb){
  let query = _.pick(config.bootstrap.defaultPoll2);
  Poll.findOrCreate(config.bootstrap.defaultPoll2,config.bootstrap.defaultPoll2, function(err, poll, created) {
    console.log("Created poll:", created);
    cb(err);
  });
};

const createPoll3 = function(cb){
  let query = _.pick(config.bootstrap.defaultPoll3);
  Poll.findOrCreate(config.bootstrap.defaultPoll3,config.bootstrap.defaultPoll3, function(err, poll, created) {
    console.log("Created poll:", created);
    cb(err);
  });
};

const createPoll4 = function(cb){
  let query = _.pick(config.bootstrap.defaultPoll4);
  Poll.findOrCreate(config.bootstrap.defaultPoll4,config.bootstrap.defaultPoll4, function(err, poll, created) {
    console.log("Created poll:", created);
    cb(err);
  });
};
function bootstrap(done) {
  console.log("Going to run bootstrap")

  async.parallel([ 
    createPoll1,
    createPoll2,
    createPoll3,
    createPoll4
  ],
  function (err, finalCb) {    
    if(typeof done === 'function'){
      done();
    }
  });
};

module.exports = bootstrap;
