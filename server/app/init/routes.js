module.exports = function(app) {
  // Load routes for various modules
  require('../routes/user')(app);
  require('../routes/poll')(app);
};
