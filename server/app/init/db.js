var config = require('../../config/appConfig');
var mongoose = require('mongoose');

mongoose.connect(config.db.connectionString);

module.exports.mongoose = mongoose;
