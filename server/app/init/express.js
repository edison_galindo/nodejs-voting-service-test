//-------------------------------load modules---------------------------------\\
var express      = require('express');
var mime         = require('mime');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var i18n         = require('i18n');
var cors         = require('cors');
var engines      = require('consolidate');
var config       = require('../../config/appConfig');

module.exports = function() {

  //--------------------setup the express application-------------------------\\
  var app = express();

  i18n.configure({
    // setup some locales - other locales default to en silently
    locales: ['en', 'es'],
    defaultLocale: 'en',
    directory: __dirname + '/locales'
  });

  app
  .use(logger('dev')) 
  .use(cookieParser())            //use cookie - needed for auth
  .use(bodyParser.json({          // to support JSON-encoded bodies
    'limit': '50mb'
  }))
  .use(bodyParser.urlencoded({    // to support URL-encoded bodies
    'limit': '50mb',
    'extended': false
  }))
  .use(cors({ 'credentials': true, 'origin': true}));
  
  //--------------------------Add pagination support to mongoose  -------------------------------\\
  require('mongoose-pagination');

  // //--------------------------  CORS  -------------------------------\\
  // app.all('/', function(req, res, next) {
  //   res.header("Access-Control-Allow-Origin", "*");
  //   res.header("Access-Control-Allow-Headers", "X-Requested-With");
  //   next();
  // });

  //---------------------update host value in config--------------------------\\
  app.use(function(req, res, next) {
    config.web.host = req.headers.host;
    next();
  });

  app.use(express.static('public'));
  app.use(i18n.init);

  //--------------------------initialize routes-------------------------------\\
  require('./routes')(app);

  //--------------------------initialize custom responses---------------------\\
  require('express-custom-response')(__dirname + '/../responses');

  //-----------------------------start server---------------------------------\\
  app.listen(config.web.port, function() {
    console.log("> Poll API Server is running on port: " + config.web.port);
  });

  app.set('views', __dirname + '/../views');
  app.engine('html', engines.mustache);
  app.set('view engine', 'html');

  return app;
};
