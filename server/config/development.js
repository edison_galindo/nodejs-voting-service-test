module.exports = {
  'web': {
    'host': 'localhost',
    'port': process.env.PORT || 4000,
    'sessionSecret': 'zemogapollsecret',
    'tokenSecret': 'tzemogapollsecret',
    'timeouts': {
      'session': 3600,
      'resetPassword': '1d',
      'activation': '2 days'
    },
    'fileStorageDir': '../../files'
  },
  'jwt': {
    'tokenLife': 63072000,
    'url': '/api/v1/login',
    'tokenKey': 'token'
  },
  'authentication':{
    'strategy': 'jwt',//jwt or oauth
    'tokenSecret': 'tr3lsecret'
  },
  'host': {
    'api' : 'http://localhost:3000',
    'front': 'http://localhost:4000',
  },
  'db': {
    'connectionString': process.env.DB_URL || 'mongodb://localhost:27017/zemoga-poll'
  },
  'poll': {
    'maxVotesPerUser': 3
  },
  'contact': {
    'email': 'soporte@zemoga-poll.co'
  },
  'email': {
    'fromEmail': 'no-reply@zemoga-poll.co',
    'fromName': 'An Awesome Admin',
    'apiKey': 'an-api-key'
  },
  'cdn': {
    'url': 'https://s3.amazonaws.com/our-cloud-bucket/'
  },
  'user': {
    'defaultImage': 'user1.png'
  },
  'facebook': {
    'clientId': 'facebook_client_id',
    'secret': 'facebook_secret'
  },
  'defaultPagination': {
    'page': 1,
    'limit': 10
  },
  'activationUrl': 'http://localhost:4000/account/activation',
  'bootstrap':{
    'defaultPoll1': {
      'title': 'Kanye West' ,
      'description': 'What is wrong with this guy?'
    },
    'defaultPoll2': {
      'title': 'Mark Zuckerberg' ,
      'description': 'Yeah, yeah, we care about your privacy'
    },
    'defaultPoll3': {
      'title': 'Cristina Fernández de Kirchner' ,
      'description': 'Argentine politician who served as President of Argentina from 2007 to 2015'
    },
    'defaultPoll4': {
      'title': 'Malala Yousafzai' ,
      'description': 'We realize the importance of our voices only when we are silenced'
    }
  }
};
