# A simple vote system

A simple solution to the Node.js test.  It contains the two projects, client and server, which work independently.

Please keep in mind that it is not the way I currently structure my projects and it was only done in this way for the purposes of the test.

My suggestion is a microservice architecture where authentication management is not built into the API. In my projects for use in production, I implemented a complete microservice architecture with an authentication microservice, a gateway and an IAM microservice to manage permissions in a granular way and delegate the scopes to clients..

## How to run

There are two ways, with docker-compose or like isolated apps.


### Run without docker

#### Backend
A nodejs server with a few examples of unit tests and ApiDoc.

```sh
$ cd server
$ npm i
$ npm start
```
Unit Tests:
```sh
$ npm test
```
Apidoc:
Apidoc is generated once run 'npm i' and is available in:
http://localhost:4000/api

#### Frontend
A simple ReactJs app. Nothing fancy, without styles or good practices. There are not an API requests service or a validations component. Actually, the purpose of this client is only provide a way to test the backend services. 


```sh
$ cd client
$ yarn
$ npm start
```
Development web server:
http://localhost:3000

Notes:

  - You must register an account to be able to vote.
  - Yes, an input field for the date is horrible and unuseful, I should use a datepicker to get the birthday
  - Yes, there are missing validators

### Run with docker @TODO
With small changes, this project would to run with docker-compose, the setup is almost ready, we would to change some ip addresses:
```sh
$ docker-compose up --build
```