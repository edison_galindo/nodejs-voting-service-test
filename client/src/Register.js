import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import Login from './Login';
import PollPage from './PollPage';

class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      age:'',
      marriage_status:'',
      email:'',
      password:''
    }
  }
  componentWillReceiveProps(nextProps){
    console.log("nextProps",nextProps);
  }
  handleClick(event,role){
    var apiBaseUrl = "http://localhost:4000/api/v1/";
    // console.log("values in register handler",role);
    var self = this;
    //To be done:check for empty values before hitting submit
    if(this.state.email.length>0 && this.state.password.length>0){

      let payload = {};
      if(this.state.email.length) payload.email = this.state.email;
      if(this.state.password.length) payload.password = this.state.password;
      if(this.state.age.length) payload.age = this.state.age;
      if(this.state.marriage_status.length) payload.marriage_status = this.state.marriage_status;

      axios.post(apiBaseUrl+'users', payload)
     .then(function (response) {
       // console.log(response.data.data);
       if(response.status == 200){
         localStorage.setItem('token', response.data.data.token);
         var pollScreen=[];
         pollScreen.push(<PollPage appContext={self.props.appContext}/>)
         self.props.appContext.setState({loginPage:[],pollScreen:pollScreen})
       } else{
         console.log("some error ocurred",response.data.code);
         alert("Registration has failed, some error ocurred");
       }
     })
     .catch(function (error) {
       console.log(error);
     });
    }
    else{
      console.log(this.state.age);
      console.log(this.state.marriage_status);
      console.log(this.state.email);
      console.log(this.state.password);

      alert("Email and Password are required fields");
    }

  }

  handleChange = (event, index, value) => {
    alert(value);
    alert(index);
  this.setState({value});

  }

  render() {
    // console.log("props",this.props);
    var userhintText,userLabel;
    userhintText="Enter your email"
    userLabel="Email"
    
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Register"
           />
           <TextField
             hintText={userhintText}
             floatingLabelText={userLabel}
             onChange = {(event,newValue) => this.setState({email:newValue})}
             />
           <br/>
           <TextField
             type = "password"
             hintText="Enter your password"
             floatingLabelText="Password"
             onChange = {(event,newValue) => this.setState({password:newValue})}
             />
           <br/>
           <TextField
             hintText="Enter your age"
             floatingLabelText="Age"
             onChange = {(event,newValue) => this.setState({age:newValue})}
             />
           <br/>
          
           <SelectField
            floatingLabelText="Marriage Status"
            value={this.state.marriage_status}
            onChange = {(event, index, newValue) => this.setState({marriage_status:newValue})}
           >
            <MenuItem value="Married" primaryText="Married" />
            <MenuItem value="Single" primaryText="Single" />
            <MenuItem value="Divorced" primaryText="Divorced" />
            <MenuItem value="Widowed" primaryText="Widowed" />
            <MenuItem value="Other" primaryText="Other" />
          </SelectField>
          <br/>
           <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
          </div>
         </MuiThemeProvider>
      </div>
    );
  }
}

const style = {
  margin: 5,
};

export default Register;
