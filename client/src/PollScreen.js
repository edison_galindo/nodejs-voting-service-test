import React, { Component } from 'react';
import './App.css';
/*
Screen:LoginScreen
Loginscreen is the main screen which the user is shown on first visit to page and after
hitting logout
*/
import LoginScreen from './Loginscreen';
/*
Module:Material-UI
Material-UI is used for designing ui of the app
*/
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';
import axios from 'axios';

var apiBaseUrl = "http://localhost:4000/api/v1/";

/*
Module:superagent
superagent is used to handle post/get requests to server
*/
var request = require('superagent');

class PollScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      polls: [],
      isLoading: false,
      draweropen:false,
      votecount:10,
      printingmessage:'',
      printButtonDisabled:false,
      error: null,
    }
  }

  componentWillMount(){
    // console.log("prop values",this.props.role);
    var votecount = 3;
    
    this.setState({votecount});

    this.getPolls();
  }

  getPolls() {
    this.setState({ isLoading: true });

    axios.get(apiBaseUrl + 'poll/my-votes', {headers: {token: localStorage.getItem('token')}})
      .then(result => this.setState({
        polls: result.data.data,
        isLoading: false
      }))
      .catch(error => this.setState({
        error,
        isLoading: false
      }));
  }
  
  /*
    Function:handleClick
    Parameters: event
    Usage:This fxn is handler of submit button which is responsibel fo rhandling file uploads
    to backend
  */
  handleClick(id, type){
    var self = this;
    var payload={
      "type": type,
      "id": id
    }
    axios.post(apiBaseUrl+'poll/vote', payload, {headers: {token: localStorage.getItem('token')}})
   .then(function (response) {
     console.log(response);
     if(response.status == 200){
       console.log("Login successfull", response.data.access_token);
       self.getPolls();
       // update localStorage
       // var pollScreen=[];
       // pollScreen.push(<PollPage appContext={self.props.appContext}/>)
       // self.props.appContext.setState({loginPage:[],pollScreen:pollScreen})
     }
     else if(response.data.code == 204){
       console.log("Username password do not match");
       alert(response.data.success)
     }
     else{
       console.log("Username does not exists");
       alert("Username does not exist");
     }
   })
   .catch(function (error) {
     console.log(error);
   });
  }
  /*
    Function:toggleDrawer
    Parameters: event
    Usage:This fxn is used to toggle drawer state
    */
  toggleDrawer(event){
    // console.log("drawer click");
    this.setState({draweropen: !this.state.draweropen})
  }
  /*
    Function:toggleDrawer
    Parameters: event
    Usage:This fxn is used to close the drawer when user clicks anywhere on the 
    main div
    */
  handleDivClick(event){
    // console.log("event",event);
    if(this.state.draweropen){
      this.setState({draweropen:false})
    }
  }
  /*
    Function:handleLogout
    Parameters: event
    Usage:This fxn is used to end user session and redirect the user back to login page
    */
  handleLogout(event){
    // console.log("logout event fired",this.props);
    localStorage.removeItem('token');
    var loginPage =[];
    loginPage.push(<LoginScreen appContext={this.props.appContext}/>);
    this.props.appContext.setState({loginPage:loginPage,pollScreen:[]})
  }

  render() {
    return (
      <div className="App">
        <div onClick={(event) => this.handleDivClick(event)}>
          <center>
            <div>
              Up or down
            </div>
          </center>
        <div>
          {this.state.printingmessage}
        </div>
        <div>
          {this.state.polls.map(poll =>
            <div key={poll.id} style={box}>
              <div style={data}>
                <p>{poll.title}</p>
                <p>Up: {poll.upTotal}</p>
                <p>Down: {poll.downTotal}</p>
                <p>My Votes: {poll.myVotes}</p>
              </div>
               <div style={actions}>
              <MuiThemeProvider>
                <RaisedButton label="Up" disabled={poll.myVotes >= 3} primary={true} style={style} onClick={() => this.handleClick(poll.id, 'up')}/>
              </MuiThemeProvider>
            
              <MuiThemeProvider>
                <RaisedButton  disabled={poll.myVotes >= 3} label="Down" primary={true} style={style} onClick={() => this.handleClick(poll.id, 'down')}/>
              </MuiThemeProvider>
              </div>
            </div>
          )}
        </div>
        </div>
      </div>
    );
  }
}

const style = {
  margin: 5,
};

const data = {
  margin: 5,
  width: '100%',
};

const actions = {
  margin: 5,
  width: '100%',
};

const box = {
  width: '40%',
  float: 'left',
  border: '1px solid #000000',
  margin: '5px'
};

export default PollScreen;